medieval_settlements <- read.csv(
  "data-raw/medieval_settlements.csv", 
  header = T, 
  stringsAsFactors = F,
  encoding = "UTF-8"
)

usethis::use_data(medieval_settlements, overwrite = T)
