---
title: "Cultural distance: Small scale case study, burial ground Szintlorenc (Hungary)"
author: "Agnes Schneider, Petr Pajdla"
date: "August 21, 2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# LOAD LIBRARIES #### 

```{r}
# devtools::install_git("https://gitlab.com/CRC1266-A2/moin")
library(moin)
```

# LOAD DATA #### 

```{r}
graves_SZL <- read.csv("DATA/moin2.0_data.csv", header = TRUE, row.names=1)

# INSPECT DATA####
# str(graves_SZL)
```

## DATA PREPARATION
```{r}
# features
fea <- tidyr::gather(graves_SZL, colnames(graves_SZL[, 3:ncol(graves_SZL)]), key=  
                          "type_col", value = "count")
fea <- fea[fea$count > 0, ]
fea <- fea[, 1:3]
colnames(fea) <- c("x", "y", "type")
fea$x <- as.integer(round(fea$x, 0))
fea$y <- as.integer(round(fea$y, 0))

# nodes
no <- cbind(graves_SZL[,1:2], rownames(graves_SZL))
colnames(no) <- c("nodes_x", "nodes_y", "node_id")
rownames(no) <- NULL
no$node_id <- 1:nrow(no)

```

# CULTURAL DISTANCE CALCULATION #### 

## Cultural distance

Function `moin_cult_dist()` returns distance matrix of `iom` type, i.e. full graph (n to n graph using
intervening opportunity method). The function produces a plot of voronoi thesselation as a 
check, the attributes (features) are correctly mapped to the nodes.

```{r warning=FALSE, message=FALSE} 
gr_dist <- moin_cult_dist(nodes_x = no$nodes_x, nodes_y = no$nodes_y, nodes_id = no$node_id,  
               features = fea, type_col = "type", label = rownames(graves_SZL))
``` 

```{r}
gr_dist
```


## Cultural distance network function

Function `moin_cult_network()` is a wrapper function around function `moin_cult_dist()`,
but the wrapper function returns truncated distance matrix that is generated based on a model 
graph (option `network_method`, defaults to `iom`). The truncated distance matrix contains `NA` for the edges that 
are not present in the graph, whereas the `moin_cult_dist()` outputs adjacency matrix with zeros for the plotting reasons.
In case of a cemetery using any other options of `network_method` argument than `iom` seems unneccessary.

```{r}
gr_net <- moin_cult_network(nodes_x = no$nodes_x, nodes_y = no$nodes_y, nodes_id = no$node_id,  
               features = fea, type_col = "type", label = rownames(graves_SZL), network_method = "ppa", par = 3) 
```

```{r}
gr_net
```

```{r}
gr_dist$dist_matr %>% hist()

index <- gr_dist$dist_matr < unname(quantile(gr_dist$dist_matr)[2])
index <- gr_dist$dist_matr < 1
matrix <- gr_dist$dist_matr

matrix[index] <- NA_integer_

plot(igraph::graph_from_adjacency_matrix(matrix))
```


<!-- ```{r} -->
<!-- geogr_dist <- dist(cbind(graves_SZL$X_coord, graves_SZL$Y_coord), method = "euclidean", upper = TRUE, diag = TRUE) -->
<!-- plot(cor(gr_dist$dist_matr,  as.matrix(geogr_dist)),  -->
<!--      xlim = c(-0.4, 0.5), ylim = c(-0.4, 0.5),  -->
<!--      xlab = "cultural distance", ylab = "spatial distance") -->
<!-- ``` -->

<!-- ```{r} -->
<!-- cor_matr <- cor(gr_dist$dist_matr, as.matrix(geogr_dist)) -->
<!-- reshape2::melt(cor_matr) -->
<!-- library(ggplot2) -->
<!-- ggplot(cor_matr, aes(x = )) -->
<!-- ``` -->


## Plotting the graphs of cultural distances

```{r}
plot(gr_dist)
plot(gr_net)
```


## Plotting the cultural distance using heatmap

Cultural distance can be plotted using heatmaps.

```{r}
heatmap(gr_dist$dist_matr)
# heatmap(gr_net$trunc_dist_matr)
```

## Plotting the *typespectrum* plot

Plotting the typespectrum plot is useful to be able to compare the *typespectra* between nodes, i.e. 
the compositions of artefact assemblage of individual nodes (graves in our case, can be sites too).

We implement seriation in order to order the individual typespectra plots in a way 
that more similar typespectra are closer together.

```{r}
# seriation is performed on the typespectra matrix in order to arrange nodes
# in a way that nodes with similar typespectra are closer together
typespec_matrix <- as.matrix(gr_dist$type_spectra[, -1])
rownames(typespec_matrix) <- as.character(gr_dist$input_data$label)

# filter nodes with certain number of types in a spectrum to show in the plot
typespec_matrix_filt <- typespec_matrix[rowSums(typespec_matrix) > 10, ]

# seriate on a typespectra matrix
node_order <- seriation::get_order(seriation::seriate(typespec_matrix_filt))

# create an ordered vector of node names to plot a seriated facetted plot later
node_name <- factor(reorder(rownames(typespec_matrix_filt), node_order), ordered = TRUE)

# remove rownames from the matrix to simplify creating dataframe
rownames(typespec_matrix_filt) <- c()

# node order added to typespectra matrix
typespec_df <- data.frame(node_name = node_name,
                          typespec_matrix_filt)

# type_spectra matrix is gathered as a data frame for plotting
types_per_node <- tidyr::gather(typespec_df, key = "type", value = "count", -c(1:2))
```

```{r typespectra-plot, fig.height=12, fig.width=12}
library(ggplot2)
ggplot(types_per_node, aes(x = type, y = count)) +
  geom_bar(stat = "identity") +
  facet_wrap(~node_name, ncol = 1, strip.position = "left", as.table = FALSE) +
  theme(panel.grid.minor = element_blank())
```






