austria <- read.table("./AT_Austria.csv",
                      header=TRUE,
                      sep=",")
austria <- austria[,1:6]
Austria<-austria[austria$Origin!=austria$Destination,]
AustriaExp <- glm(Data~Origin+Destination+log(Dij),
                  family = poisson(link = "log"),
                  data = Austria)
summary(AustriaExp)

Austria$fitted1 <- round(AustriaExp$fitted.values,2)

Aust_cor <- cor(Austria$Data,
                Austria$fitted1,
                method = "pearson",
                use = "complete")
Aust_rsq <- Aust_cor * Aust_cor

##########################################################################
#This block of code will calculate balancing factors for an entropy
#maximising model (doubly constrained)
#set beta to the appropriate value according to whether exponential or power
if(tail(names(coef(AustriaExp)),1)=="Dij"){
  Austria$beta <- coef(AustriaExp)["Dij"]
  disdecay = 0
} else {
  Austria$beta <- coef(AustriaExp)["log(Dij)"]
  disdecay = 1
}
#Create some new Ai and Bj columns and fill them with starting values – set up
#a variable called diff for testing convergence
Austria$Ai <- 1
Austria$Bj <- 1
Austria$OldAi <- 10
Austria$OldBj <- 10
Austria$diff <- abs((Austria$OldAi-Austria$Ai)/Austria$OldAi)
#create convergence and iteration variables and give them initial values
cnvg = 1
its = 0
#This is a while-loop which will calculate origin and destination balancing
#factors until the specified convergence criterion is met
while(cnvg > 0.001){
  its = its + 1 #increment the iteration counter by 1
  #First some initial calculations for Ai...
  if(disdecay==0){
    Austria$Ai <- (Austria$Bj*Austria$Dj*exp(Austria$Dij*Austria$beta))
  } else {
    Austria$Ai <- (Austria$Bj*Austria$Dj*exp(log(Austria$Dij)*Austria$beta))
  }
  #aggregate the results by your origins and store in a new data frame
  AiBF <- aggregate(Ai ~ Origin, data = Austria, sum)
  #now divide by 1
  AiBF$Ai <- 1/AiBF$Ai
  #and replace the initial values with the new balancing factors
  updates = AiBF[match(Austria$Origin,AiBF$Origin),"Ai"]
  Austria$Ai = ifelse(!is.na(updates), updates, Austria$Ai)
  #now, if not the first iteration, calculate the difference between the
  #new Ai values and the old Ai values and once complete, overwrite the old Ai
  #values with the new ones.
  if(its==1){
    Austria$OldAi <- Austria$Ai
  } else {
    Austria$diff <- abs((Austria$OldAi-Austria$Ai)/Austria$OldAi)
    Austria$OldAi <- Austria$Ai
  }
  #Now some similar calculations for Bj...
  if(disdecay==0){
    Austria$Bj <- (Austria$Ai*Austria$Oi*exp(Austria$Dij*Austria$beta))
  } else {
    Austria$Bj <- (Austria$Ai*Austria$Oi*exp(log(Austria$Dij)*Austria$beta))
  }
  #aggregate the results by your destinations and store in a new data frame
  BjBF <- aggregate(Bj ~ Destination, data = Austria, sum)
  #now divide by 1
  BjBF$Bj <- 1/BjBF$Bj
  #and replace the initial values by the balancing factor
  updates = BjBF[match(Austria$Destination,BjBF$Destination),"Bj"]
  Austria$Bj = ifelse(!is.na(updates), updates, Austria$Bj)
  #now, if not the first iteration, calculate the difference between the
  #new Bj values and the old Bj values and once complete, overwrite the old Bj
  #values with the new ones.
  if(its==1){
    Austria$OldBj <- Austria$Bj
  } else {
    Austria$diff <- abs((Austria$OldBj-Austria$Bj)/Austria$OldBj)
    Austria$OldBj <- Austria$Bj
  }
  #overwrite the convergence variable
  cnvg = sum(Austria$diff)
}

if(disdecay==0){
  Austria$SIM_Estimates <-
    (Austria$Oi*Austria$Ai*Austria$Dj*Austria$Bj*exp(Austria$Dij*Austria$beta))
} else{
  Austria$SIM_Estimates_pow <-
    (Austria$Oi*Austria$Ai*Austria$Dj*Austria$Bj*exp(log(Austria$Dij)*Austria$beta))
}

Austria_distmat <- moin:::flow_mat_function(flow_table = austria,
                                            origin = "Origin",
                                            destination = "Destination",
                                            flow = "Dij")

Austria_flow_table <- moin:::flow_table_function(distmat = Austria_distmat,
                                                 push_attributes = unique(austria$Oi),
                                                 pull_attributes = unique(austria$Dj))

plot(Austria$Data, Austria_flow_table$Data)
cor(Austria_flow_table$Data,Austria$Data)

Austria_moin <- moin::moin_interaction(
  distance_matrix = Austria_distmat,
  push_attributes = unique(austria$Oi),
  pull_attributes = unique(austria$Dj))

cor(Austria_moin$Data,Austria$Data)
plot(Austria$SIM_Estimates_pow, Austria_moin$SIM_Estimates_pow)


## for(i in 1:100) {
##   set.seed(i)
##   Austria_moin <- moin::moin_interaction(
##   distance_matrix = Austria_distmat,
##   push_attributes = unique(austria$Oi),
##   pull_attributes = unique(austria$Dj))

##   print(cor(Austria_moin$Data,Austria$Data))
##   }
