library(magrittr)

settlements <- moin::medieval_settlements %>%
  sf::st_as_sf(coords = c("x","y"),
               remove = FALSE,
               crs = 25832)

site_network <- moin::moin_network(
  input = settlements,
  method = "gabriel")


## old approach
res <- moin::moin_interaction(
  distance_matrix = site_network$truncated_distance_matrix,
  deterrence_function = moin::moin_det_exp,
  push_attributes = site_network$input_data$rank,
  pull_attributes = site_network$input_data$betweenness
)

## updated approach based on dennett
flow_table_function <- function(distmat,
                                push_attributes,
                                pull_attributes,
                                cnvg_threshold = 0.1,
                                initial_Ai = 10,
                                initial_Bj = 10) {
  push_attributes <- as.integer(round(push_attributes/max(push_attributes) * 10))
  pull_attributes <- as.integer(round(pull_attributes/max(pull_attributes) * 10))

  from_list <- as.list(1:length(push_attributes))
  from_to_lists <- lapply(
    X = from_list,
    FUN = function(from) {
      flow_table <- data.frame(from = rep(as.character(from), length(push_attributes)),
                               to = rep(as.character(0), length(push_attributes)),
                               Oi = rep(0, length(push_attributes)),
                               Dj = rep(0, length(push_attributes)),
                               Dij = rep(0, length(push_attributes)))                               
      
      for(j in 1:length(push_attributes)) {
        flow_table$to[j] <- as.character(j)
        flow_table$Dj[j] <- pull_attributes[j]
        flow_table$Oi[j] <- push_attributes[from]
        flow_table$Dij[j] <- distmat[from,j]
        flow_table$Data[j] <- 0
      }      
      
      flow_table$from <- as.character(flow_table$from)
      flow_table$to <- as.character(flow_table$to)
      flow_table$Oi <- as.integer(flow_table$Oi)
      flow_table$Dj <- as.integer(flow_table$Dj)      
      flow_table$Data <- as.integer(flow_table$Data)

      return(flow_table)
    }
  )
  flow_table <- do.call("rbind",from_to_lists)

  flow_table$Ai <- initial_Ai
  flow_table$Bj <- initial_Bj
  flow_table$OldAi <- initial_Ai
  flow_table$OldBj <- initial_Bj
  flow_table$diff <- abs((flow_table$OldAi-flow_table$Ai)/flow_table$OldAi)
  
  cnvg = 1
  its = 0

  #This is a while-loop which will calculate origin and destination balancing
  #factors until the specified convergence criterion is met
  while(cnvg > cnvg_threshold){
    its = its + 1 #increment the iteration counter by 1

    flow_table$Ai <- flow_table$Bj*flow_table$Dj
    
    #aggregate the results by your origins and store in a new data frame
    AiBF <- aggregate(Ai ~ from, data = flow_table, sum)
    
    ## ## equivalent
    ## aggregate(x = flow_table$Ai,
    ##           by = list(from = flow_table$from),
    ##           FUN = "sum")

    ## ## data.table version
    ## data.table::data.table(flow_table)[, .(Ai=sum(Ai)), by=from]
    
    #now divide by 1
    AiBF$Ai <- ifelse(AiBF$Ai==0, 1e-3, 1/AiBF$Ai)
    
    #and replace the initial values with the new balancing factors
    updates = AiBF[match(flow_table$from,AiBF$from),"Ai"]
    flow_table$Ai = ifelse(!is.na(updates), updates, flow_table$Ai)
    #now, if not the first iteration, calculate the difference between the
    #new Ai values and the old Ai values and once complete, overwrite the old Ai
    #values with the new ones.
    if(its==1){
      flow_table$OldAi <- flow_table$Ai
    } else {
      flow_table$diff <- abs((flow_table$OldAi-flow_table$Ai)/flow_table$OldAi)
      flow_table$OldAi <- flow_table$Ai
    }
    
    flow_table$Bj <- (flow_table$Ai*flow_table$Oi)
    
    #aggregate the results by your destinations and store in a new data frame
    BjBF <- aggregate(Bj ~ to, data = flow_table, sum)

    #now divide by 1
    BjBF$Bj <- ifelse(BjBF$Bj==0, 1e-3, 1/BjBF$Bj)
    
    #and replace the initial values by the balancing factor
    updates = BjBF[match(flow_table$to,BjBF$to),"Bj"]
    flow_table$Bj = ifelse(!is.na(updates), updates, flow_table$Bj)
    #now, if not the first iteration, calculate the difference between the
    #new Bj values and the old Bj values and once complete, overwrite the old Bj
    #values with the new ones.
    if(its==1){
      flow_table$OldBj <- flow_table$Bj
    } else {
      flow_table$diff <- abs((flow_table$OldBj-flow_table$Bj)/flow_table$OldBj)
      flow_table$OldBj <- flow_table$Bj
    }
    #overwrite the convergence variable
    cnvg = sum(flow_table$diff)
  }

  flow_table$Data <- flow_table$Oi * flow_table$Ai * flow_table$Dj * flow_table$Bj

  
  relevant_columns <- c("from","to","Data","Oi","Dj","Dij") 
  return(subset(flow_table, select = relevant_columns))
  ## equivalent:
  ## return(within(flow_table, rm("Ai","Bj","OldAi","OldBj","diff")))
  
}

flow_table <- flow_table_function(
  distmat = site_network$truncated_distance_matrix,
  push_attributes = site_network$input_data$rank,
  pull_attributes = site_network$input_data$betweenness  
)

flow_table <- flow_table[!is.na(flow_table$Dij),]


## adjust scale and make integer
flow_table$Data[flow_table$Data==0] <- 1
flow_table$Data <- as.integer(round(flow_table$Data * 1000))
flow_table$Oi <- as.integer(flow_table$Oi * 1000)
flow_table$Dj <- as.integer(flow_table$Dj * 1000)

site_networkExp <- glm(Data~from+to+log(Dij),
                       family = poisson(link = "log"),
                       data = flow_table)
summary(site_networkExp)

flow_table$fitted1 <- round(site_networkExp$fitted.values,2)

flow_table_cor <- cor(flow_table$Data,
                      flow_table$fitted1,
                      method = "pearson",
                      use = "complete")
flow_table_rsq <- flow_table_cor * flow_table_cor

##########################################################################
#This block of code will calculate balancing factors for an entropy
#maximising model (doubly constrained)
#set beta to the appropriate value according to whether exponential or power
if(tail(names(coef(site_networkExp)),1)=="Dij"){
  flow_table$beta <- coef(site_networkExp)["Dij"]
  disdecay = 0
} else {
  flow_table$beta <- coef(site_networkExp)["log(Dij)"]
  disdecay = 1
}


#Create some new Ai and Bj columns and fill them with starting values – set up
#a variable called diff for testing convergence
flow_table$Ai <- 10
flow_table$Bj <- 10
flow_table$OldAi <- 10
flow_table$OldBj <- 10
flow_table$diff <- abs((flow_table$OldAi-flow_table$Ai)/flow_table$OldAi)
#create convergence and iteration variables and give them initial values
cnvg = 1
its = 0
#This is a while-loop which will calculate origin and destination balancing
#factors until the specified convergence criterion is met
while(cnvg > 0.1){
  its = its + 1 #increment the iteration counter by 1
  #First some initial calculations for Ai...
  if(disdecay==0){
    flow_table$Ai <- (flow_table$Bj*flow_table$Dj*exp(flow_table$Dij*flow_table$beta))
  } else {
    flow_table$Ai <- (flow_table$Bj*flow_table$Dj*exp(log(flow_table$Dij)*flow_table$beta))
  }
  #aggregate the results by your origins and store in a new data frame
  AiBF <- aggregate(Ai ~ from, data = flow_table, sum)
  
  ## ## equivalent
  ## aggregate(x = flow_table$Ai,
  ##           by = list(from = flow_table$from),
  ##           FUN = "sum")

  ## ## data.table version
  ## data.table::data.table(flow_table)[, .(Ai=sum(Ai)), by=from]
  
  ## #now divide by 1
  ## AiBF$Ai <- 1/AiBF$Ai

  #now divide by 1
  AiBF$Ai <- ifelse(AiBF$Ai==0, 1e-3, 1/AiBF$Ai)
  
  #and replace the initial values with the new balancing factors
  updates = AiBF[match(flow_table$from,AiBF$from),"Ai"]
  flow_table$Ai = ifelse(!is.na(updates), updates, flow_table$Ai)
  #now, if not the first iteration, calculate the difference between the
  #new Ai values and the old Ai values and once complete, overwrite the old Ai
  #values with the new ones.
  if(its==1){
    flow_table$OldAi <- flow_table$Ai
  } else {
    flow_table$diff <- abs((flow_table$OldAi-flow_table$Ai)/flow_table$OldAi)
    flow_table$OldAi <- flow_table$Ai
  }
  #Now some similar calculations for Bj...
  if(disdecay==0){
    flow_table$Bj <- (flow_table$Ai*flow_table$Oi*exp(flow_table$Dij*flow_table$beta))
  } else {
    flow_table$Bj <- (flow_table$Ai*flow_table$Oi*exp(log(flow_table$Dij)*flow_table$beta))
  }
  #aggregate the results by your destinations and store in a new data frame
  BjBF <- aggregate(Bj ~ to, data = flow_table, sum)

  ## #now divide by 1
  ## BjBF$Bj <- 1/BjBF$Bj

  #now divide by 1
  BjBF$Bj <- ifelse(BjBF$Bj==0, 1e-3, 1/BjBF$Bj)
  
  #and replace the initial values by the balancing factor
  updates = BjBF[match(flow_table$to,BjBF$to),"Bj"]
  flow_table$Bj = ifelse(!is.na(updates), updates, flow_table$Bj)
  #now, if not the first iteration, calculate the difference between the
  #new Bj values and the old Bj values and once complete, overwrite the old Bj
  #values with the new ones.
  if(its==1){
    flow_table$OldBj <- flow_table$Bj
  } else {
    flow_table$diff <- abs((flow_table$OldBj-flow_table$Bj)/flow_table$OldBj)
    flow_table$OldBj <- flow_table$Bj
  }

  #overwrite the convergence variable
  #cnvg = sum(flow_table$diff)
  cnvg = sum(flow_table$diff[flow_table$diff<Inf], na.rm = TRUE)
}

if(disdecay==0){
  flow_table$SIM_Estimates <-
    (flow_table$Oi*flow_table$Ai*flow_table$Dj*flow_table$Bj*exp(flow_table$Dij*flow_table$beta))
} else{
  flow_table$SIM_Estimates_pow <-
    (flow_table$Oi*flow_table$Ai*flow_table$Dj*flow_table$Bj*exp(log(flow_table$Dij)*flow_table$beta))
}




flow_mat_function <- function(flow_table) {
  distmat <- matrix(data=0,
                    nrow = length(unique(flow_table$from)),
                    ncol = length(unique(flow_table$from)))
  for (i in 1:dim(distmat)[1]) {
    for (j in 1:dim(distmat)[2]) {
      
      if (length(flow_table$SIM_Estimates_pow[flow_table$from==i & flow_table$to==j]) > 0) {
        distmat[i,j] <- flow_table$SIM_Estimates_pow[flow_table$from==i & flow_table$to==j]
      }      
    }
  }

  return(distmat)  
}


test <- moin::flows_to_line(spatial_reference_points = settlements,
                            connections = site_network$truncated_distance_matrix,
                            flow_matrix = flow_mat_function(flow_table),
                            drop_connection_value = 0)

plot(sf::st_geometry(test), lwd=(unlist(test$V1)/max(unlist(test$V1)))*2)

## Use n equally spaced breaks to assign each value to n-1 equal sized bins
values <- site_network$input_data$betweenness/max(site_network$input_data$betweenness)
ii <- cut(values, breaks = seq(min(values), max(values), len = 100), 
          include.lowest = TRUE)
## Use bin indices, ii, to select color from vector of n-1 equally spaced colors
colors <- colorRampPalette(c("lightgreen", "darkgreen"))(99)[ii]

plot(sf::st_geometry(site_network$input_data), add=TRUE,
     pch = 20,
     cex = site_network$input_data$rank / 2 + 1,
     col = colors)
     
