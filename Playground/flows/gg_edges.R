#' gg_edges
#'
#' @param sample sample of point pattern, class is sf; id-column required
#' @param trunc_mat truncated distance matrix of the point pattern
#'
#' @return returns sf object of the edges defined by igraph
gg_edges <- function(sample, trunc_mat){
  edges_sf <- lapply(X = split(sample, seq(1, nrow(sample))),
                     FUN = function(sample_p, sample, trunc_mat){
                       first_i <- sample_p$id
                       n <- which(!is.na(trunc_mat[first_i,]))
                       lapply(X = as.list(n),
                              FUN = function(x, sample_p, sample){
                                ma <- matrix(c(sf::st_coordinates(sample_p), sf::st_coordinates(sample[x,])), ncol = 2, byrow = TRUE)
                                return(ma)
                                },
                              sample_p = sample[first_i,],
                              sample = sample)
                       },
                     sample = sample,
                     trunc_mat = trunc_mat) %>%
    unlist(recursive = FALSE) %>%
    sf::st_multilinestring() %>%
    sf::st_sfc() %>%
    sf::st_sf()

  return(edges_sf)
}
