## TODOs:
## - move/separate the code: functions go in to R folder into a files named "flows_XXX" while examples (under "function testing") can remain in this file
## - the add_values function is not finished! (only method circ is implemented yet...!)
## - for what is function "gg_edges" used?
## - path_moin and path are not choosing the same ways --> why??
## FUNCTION options:
## - are there central points for the "hex" and "random" functions? these would be necessary to compare circle (and "circular_sample") with the other stuff; perhaps write a wrapper around the st_sample functions to make sure that the central point is stored somewhere and can be retrieved later; in the random case just add the "mean center", i.e. c(sum(x_coords)/length(x_coords), sum(y_coords)/length(y_coords))

library(magrittr)
library(ggplot2)
# function testing --------------------------------------------------------

# make samples
box <- list(rbind(c(0,0), c(100000,0), c(100000,100000), c(0,100000), c(0,0))) %>%
  sf::st_polygon()

sample_rand <- sf::st_sample(x = box, size = 150, type = "random") %>%
  sf::st_sf()

ggplot(data = sample_rand) +
  geom_sf(mapping = aes(), size = 1.7)

sample_hex <- sf::st_sample(x = box, size = 250, type = "hexagonal") %>% 
  sf::st_sf()

cent_hex <- round(nrow(sample_hex)/2)

ggplot(data = sample_hex) +
  geom_sf(mapping = aes(), size = 1.7) +
  geom_sf(sample_hex[cent_hex,], size = 1.7, col = "red", mapping = aes())

sample_reg <- sf::st_sample(x = box, size = 250, type = "regular") %>% 
  sf::st_sf()

cent_reg <- round(nrow(sample_reg)/2)

ggplot(data = sample_reg) +
  geom_sf(mapping = aes(), size = 1.7) +
  geom_sf(sample_reg[cent_reg,], size = 1.7, col = "red", mapping = aes())

cen <- sf::st_point(c(50000,50000)) %>%
  sf::st_sfc() %>%
  sf::st_sf()
n_ring <- 3
width <- c(90000, 60000, 30000)
buffer <- 5000
n_samp <- 250

sample_circ <- circular_sample(cen, n_ring, width, buffer, n_samp, plot = FALSE) %>% 
  sf::st_sf()

ggplot(data = sample_circ) +
  geom_sf(mapping = aes(), size = 1.7)

# moin rand
moin_rand <- moin::moin_network(sample_rand, method = "relativeneigh")
moin_pw_rand <- pathwork_fun(target_i = 150, in_sample = moin_rand)

ggplot() +
  geom_sf(data = moin_pw_rand$pathwork, mapping = aes()) +
  geom_sf(data = moin_pw_rand$input_data, mapping = aes(), col = "#2c7fb8") +
  geom_sf(data = moin_pw_rand$input_data[150,], mapping = aes(), col = "red") + # target point
  # geom_sf_text(data = moin_pw_rand$input_data, mapping = aes(label = n_inflows), nudge_y = -2000, size = 3)
  geom_sf_text(data = moin_pw_rand$input_data, mapping = aes(label = id), nudge_y = -2000, size = 3)

# # moin reg
# moin_reg<- moin::moin_network(sample_reg, method = "relativeneigh")
# moin_pw_reg <- pathwork_fun(target_i = 150, in_sample = moin_reg)
# 
# ggplot() +
#   geom_sf(data = moin_pw_reg$pathwork, mapping = aes()) +
#   geom_sf(data = moin_pw_reg$input_data, mapping = aes(), col = "#2c7fb8") +
#   geom_sf(data = moin_pw_reg$input_data[150,], mapping = aes(), col = "red") + # target point
#   geom_sf_text(data = moin_pw_reg$input_data, mapping = aes(label = n_inflows), nudge_y = -2000, size = 3)
# # geom_sf_text(data = moin_reg$input_data, mapping = aes(label = id), nudge_y = -2000, size = 3)

# # moin hex
# moin_hex <- moin::moin_network(sample_hex, method = "relativeneigh")
# moin_pw_hex <- pathwork_fun(target_i = 150, in_sample = moin_hex)
# 
# ggplot() +
#   geom_sf(data = moin_pw_hex$pathwork, mapping = aes()) +
#   geom_sf(data = moin_pw_hex$input_data, mapping = aes(), col = "#2c7fb8") +
#   geom_sf(data = moin_pw_hex$input_data[150,], mapping = aes(), col = "red") + # target point
#   geom_sf_text(data = moin_pw_hex$input_data, mapping = aes(label = n_inflows), nudge_y = -2000, size = 3)
# # geom_sf_text(data = moin_hex$input_data, mapping = aes(label = id), nudge_y = -2000, size = 3)

# sample_rand
pw_rand <- pathwork_fun(target_i = 150, in_sample = sample_rand)

ggplot() +
  geom_sf(data = sample_rand, mapping = aes()) +
  geom_sf(data = pw_rand$pathwork) +
  geom_sf(data = sample_rand[150,], mapping = aes(), col = "red") + # target point
  geom_sf_text(data = pw_rand$input_data, mapping = aes(label = n_inflows), nudge_y = -2000, size = 3)

# sample_circ
pw_circ <- pathwork_fun(target_i = 250, in_sample = sample_circ)

ggplot() +
  geom_sf(data = sample_circ, mapping = aes()) +
  geom_sf(data = pw_circ$pathwork) +
  geom_sf(data = sample_circ[250,], mapping = aes(), col = "red") + # target point
  geom_sf_text(data = pw_circ$input_data, mapping = aes(label = n_inflows), nudge_y = -2000, size = 3)

# # sample_reg
# pw_reg <- pathwork_fun(target_i = 240, in_sample = sample_reg)
# 
# ggplot() +
#   geom_sf(data = sample_reg, mapping = aes()) +
#   geom_sf(data = pw_reg$pathwork) +
#   geom_sf(data = sample_reg[240,], mapping = aes(), col = "red") + # target point
#   geom_sf_text(data = pw_reg$input_data, mapping = aes(label = n_inflows), nudge_y = -2000, size = 3)

# sample_hex
# pw_hex <- pathwork_fun(target_i = 123, in_sample = sample_hex)
# 
# ggplot() +
#   geom_sf(data = sample_hex, mapping = aes()) +
#   geom_sf(data = pw_hex$pathwork) +
#   geom_sf(data = sample_hex[123,], mapping = aes(), col = "red") + # target point
#   geom_sf_text(data = pw_hex$input_data, mapping = aes(label = n_inflows), nudge_y = -2000, size = 3)
