This package is the result of the joint work during two Summer Schools in 2018 and 2019. See the DESCRIPTION file for a complete list of authors.

# moin -- MOdelling INteraction.

Using the moin package you can calculate simple location and interaction models. 

In terms of theoretical models, the package allows you to calculate distance determined flows between sites. The sites can have various push or pull factors that reflect their importance.

Empirical models of interaction can be calculated using cultural distance approaches.


## Citation

- Version 2018: Daniel Knitter, Michael Bilger, Franziska Faupel, Clara Filet, Chiara Girotto, Benedikt Grammer, Wolfgang Hamer, Martin Hinz, Oliver Nakoinz, Kai Radloff, Hendrik Raese, Ray Rivers, Joe Roe, Georg Roth, Carolin Tietze, Juliane Watson, 2018. CRC1266-A2/moin: Release after Moin Summer School. doi:10.5281/zenodo.1436302  
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.1436302.svg)](https://doi.org/10.5281/zenodo.1436302)
- TODO Zenodo release of 2019 Version 

## Installation

You can install moin from github with:

``` r
if(!require('devtools')) install.packages('devtools')
devtools::install_git("https://gitlab.com/CRC1266-A2/moin")
```

## Licenses

- Text and figures:
  [CC-BY-4.0](http://creativecommons.org/licenses/by/4.0/)
  If figures are created by other people these figures are linked to their source location.

- Code:
  See the [DESCRIPTION](DESCRIPTION) file

- Data:
  [CC-0](http://creativecommons.org/publicdomain/zero/1.0/) attribution requested in reuse
