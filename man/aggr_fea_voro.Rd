% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/c_aggr_fea_voro.R
\name{aggr_fea_voro}
\alias{aggr_fea_voro}
\title{aggr_fea_voro}
\usage{
aggr_fea_voro(nodes, features, type_col, nodes_id)
}
\arguments{
\item{nodes}{a data.frame containing metric x and y coordinates of nodes, additionally an identifier. Coordinates are expected to be the first two columns.}

\item{features}{a data.frame containing metric x and y coordinates of nodes, and feature type. Coordinates are expected to be the first two columns. A small error with a sd of 0.0001 is added to the x-coordinate to avoid duplicate points.}

\item{type_col}{a character string naming the columname containing feature types.}

\item{nodes_id}{TODO}
}
\value{
a dataframe with feature types and their corresponding node. Additionally a
plot is created to display the aggregation of features to nodes. Amount of feature
per node is added as number to the plot.
}
\description{
Aggregate features by Voronoi tesselation
}
\details{
Creates a Voronoi tesselation of the nodes and aggregates features in tiles according to their spatial location. Returns a dataframe of aggregated features and their associated node as a new collum.
}
\examples{

set.seed(1234)

nodes <- data.frame(nodes_x = sample(3433806:3581396, 10, replace = TRUE),
               nodes_y = sample(5286004:5484972, 10, replace = TRUE), 
               nodes_id = c(1:10))
features <- data.frame(x = sample(3433806:3581396, 100, replace = TRUE),
                   y = sample(5286004:5484972, 100, replace = TRUE),
                   type = paste0("B", c(rep(1, 5), rep(2,15), sample(11:19, 20, replace = TRUE), 
                   sample(111:119, 30, replace = TRUE), sample(1111:1115, 30, replace = TRUE)))
                   ) 
aggr_fea <- aggr_fea_voro(nodes, features, "type", nodes_id = nodes$nodes_id) 

}
\author{
Oliver Nakoinz

Chiara Girotto

Franziska Faupel
}
